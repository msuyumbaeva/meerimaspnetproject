﻿using MeerimAspNetProject.Models;
using MeerimAspNetProject.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeerimAspNetProject.Repositories
{
    public class EmployeeRepository : Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Employees;
        }
    }
}
