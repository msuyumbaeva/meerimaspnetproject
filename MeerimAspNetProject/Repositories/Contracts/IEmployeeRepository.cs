﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MeerimAspNetProject.Models;

namespace MeerimAspNetProject.Repositories.Contracts
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
    }
}
