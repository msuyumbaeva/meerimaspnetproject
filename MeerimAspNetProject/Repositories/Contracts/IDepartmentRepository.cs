﻿using MeerimAspNetProject.Models;

namespace MeerimAspNetProject.Repositories.Contracts
{
    public interface IDepartmentRepository : IRepository<Department>
    {

    }
}