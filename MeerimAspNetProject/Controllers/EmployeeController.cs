﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MeerimAspNetProject.Repositories;

namespace MeerimAspNetProject.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public EmployeeController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var employees = unitOfWork.Employees.GetAll();
                return View(employees);
            }
        }
    }
}
